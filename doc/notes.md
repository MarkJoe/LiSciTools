lists:
  lists in theorems, definitions or examples of independant statements,
  properties or entities are denoted by lowercase letters: (a), (b), (c), ...

  list in theorems, definitions or examples of dependant or closely related
  statements like equivalences or of mathematical formulas  are denoted by
  lowercase roman numbers: (i), (ii), (iii), ...
   

pandoc filters:
   paru, a ruby binding for pandoc so that one can write pandoc filters in ruby,
      https://heerdebeer.org/Software/markdown/paru/
   GitHub ickc/pandoc-amsthm 
   GitHub chdemko/pandoc-latex-environment
   GitHub jgm/pandocfilters basic pandoc filter for theorem environments

Mathematics rendering:
   mathJax
   kaTeX

Graphics:
   GitHub jbenet/tikz2svg
   dvisvgm https://dvisvgm.de/
   GitHub yishn/tikzcd-editor
   GitHub yishn/jsx-tikzcd

Markdown preprocessors:
   PP - A generic Preprocessor (with Pandoc in mind) 
   gpp

LaTeX tools:
   ConTeXt
