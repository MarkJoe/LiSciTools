break a \section{} file into blocks
blocks are collected in an array of tupels, where each tupel consists of the following:

1. type (theorem, remark, definition, equation, axiomlist, enumerate, itemize, subsection, diagram, figure, para/text, proof, item, axiom etc...)
2. ident (label, maybe without type-prefix)
3. header/title/name
4. nr (within section, not containing section or chapter number), empty if no number given
5. content, if para/text or begin or end tag
   
there should be no nested blocks, rather start and end tags

check that the start and end blocks pair properly

possibly collect ident/label,name,header,tag,nr,etc,.. in a hash called ids, that way
some keys can be empty, depnding on what type of 