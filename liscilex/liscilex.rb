#!/usr/bin/env ruby
def prefix (string)
   if string == 'theorem' or string == 'proposition' or string == 'lemma' or string == 'corollary'
     prefix = 'thm'
   elsif string == 'definition' 
     prefix = 'def'
   elsif string == 'remark' or string == 'remarks'
     prefix = 'rem'    
   elsif string == 'equation' 
     prefix = 'eq'
   elsif string == 'example' or string == 'examples'
     prefix = 'ex'
   else 
     prefix = ''
   end
   prefix    
end

str=IO.read(ARGV[0])
##! import YAML file with environments, and types/prefixes
theoremEnvs=["definition","theorem","proposition","corollary","lemma","remark","remarks","example","examples"]
exception = false
countBegins = Hash.new
countEnds = Hash.new
blocks = [['para','','',0,str]]
newblocks  = Array.new
theoremEnvs.each do  |item|   
  countBegins[item] = 0
  j = 0
  ##! explain what blocks consist of: type, .... ,content or start/end-tags, where is label or ident?
  ##! create numbers of theorems, sections, etc, possibly insert section, chapter numbers
  newblocks =[['','','','','']]
  blocks.each do |block|
    if block[0] == "para"
      splitted = block[4].split("\\begin{#{item}}")
    else
      splitted = [block[4]]
    end 

    length = splitted.length  
    
    if length == 1
      newblocks[j] = block
      j+=1
    else
      if splitted[0] == "" 
        if splitted[1].scan(/^\[.+?\]/) == []
          newblocks[j] = [item,'','','','begin']
          newblocks[j+1] = ['para','','','',splitted[1]] 
          countBegins[item]+=1
          j+=2
        else
          newblocks[j] = [item, splitted[1].scan(/\[.+?\]/)[0].gsub(/\[/,'').gsub(/\]/,''),'','','begin'] 
          newblocks[j+1] = ['para','','','',splitted[1].gsub!(/\[.+?\]/,'')] 
          countBegins[item]+=1
          j+=2
        end
      else
        if splitted[1].scan(/^\[.+?\]/) == []
          newblocks[j]=['para','','','',splitted[0]]
          newblocks[j+1] = [item,'','','','begin']
          newblocks[j+2] = ['para','','','',splitted[1]] 
          countBegins[item]+=1
          j+=3
        else  
          newblocks[j]=['para','','','',splitted[0]]
          newblocks[j+1] = [item, splitted[1].scan(/\[.+?\]/)[0].gsub(/\[/,'').gsub(/\]/,''),'','','begin'] 
          newblocks[j+2] = ['para','','','',splitted[1].gsub!(/^\[.+?\]/,'')] 
          countBegins[item]+=1
          j+=3
        end
      end       

      2.upto(length - 1) do |k|
        if splitted[k].scan(/^\[.+?\]/) == []
          newblocks[j] = [item,'','','','begin']
          newblocks[j+1] = ['para','','','',splitted[k]] 
          countBegins[item]+=1
          j+=2
        else 
          newblocks[j] = [item, splitted[k].scan(/^\[.+?\]/)[0].gsub(/\[/,'').gsub(/\]/,''),'','','begin'] 
          newblocks[j+1] = ['para','','','',splitted[k].gsub!(/^\[.+?\]/,'')] 
          countBegins[item]+=1
          j+=2
        end
      end 
    end   
  end
  
  blocks = newblocks 

end 
  
theoremEnvs.each do  |item|   
  countEnds[item] = 0
  j = 0
  newblocks =[['','','','','']]
  blocks.each_with_index do |block,i|
    if block[0] == "para"
      splitted = block[4].split("\\end{#{item}}")
    else
      splitted = [block[4]]
    end 
  
    length = splitted.length  
    

    if block[0] != "para"
      newblocks[j] = block
      j+=1
    elsif length == 1 
      if blocks[i-1][0] != item and /\\end{#{item}}/ =~ block[4]
        abort("File not in LiSci LaTeX format! Environments #{item} do not pair properly.")
      elsif  /\\end{#{item}}/ =~ block[4]
        newblocks[j] = ['para','','','',splitted[0]] 
        newblocks[j+1] = [item,'','','','end'] 
        countEnds[item]+=1
        j+=2
      else ##! insert additional condition for other cases
        newblocks[j] = ['para','','','',splitted[0]] 
        j+=1
      end
    elsif length > 2
       abort("File not in LiSci LaTeX format! Environments #{item} do not pair properly.")
    else
      if splitted[0] == "" 
        abort("File not in LiSci LaTeX format! An environment of type #{item} is empty.")
      elsif blocks[i-1][0] != item
        ##! puts "\n " , countEnds[item], item , "\n "
        ##! print blocks[i-1][0] 
        abort("File not in LiSci LaTeX format! A #{item} environment appears to be nested.")
      else
        newblocks[j] = ['para','','','',splitted[0]] 
        newblocks[j+1] = [item,'','','','end'] 
        newblocks[j+2] = ['para','','','',splitted[1]]  
        countEnds[item]+=1
        j+=3
      end
    end 
  end
  blocks = newblocks 
end

sectionCmds=["section","chapter","divtitle","para"].each do  |item| ##! divtitle --> subsec , para --> para, but change blocktype para to text
  j = 0
  newblocks =[['','','','','']]
  blocks.each_with_index do |block,i|
    if block[0] == "para"
      ##^! check for section commands within environments! Not allowed!
      splitted = block[4].split("\\#{item}")
      if splitted.length > 1
        if splitted[0]=="" then splitted.shift end      
        titles = Array.new
        splitted.each_with_index do |string,i|
          match = string.match(/\{((?:[^{}]+|\{\g<1>\})+)\}/)
          if match == "" or match == nil
            titles[i] = ""
            splitted[i] = string
            if i == 0
              newblocks[j] = ['para','','','',splitted[i]]
              j+=1 
            else
              unless item == 'para'
                abort("File not in LiSci LaTeX format! A #{item} title appears to be missing.")
              end
              
            end
          else
            titles[i] = match[1] 
            splitted[i] = string.sub("\{"+match[1]+"\}","")
          end
        end 
        


      else

      end

      puts "\n\n"
      print splitted 
      puts "\n"
      print titles


    else
      newblocks[j] = block
      j+=1
    end 
  

    ##! make sure that sectioning command is  not within environment
  end
  # blocks = newblocks 
end
##! gives from an array of strings for each string the content of the first {... } pair 
##! sentence.split("\\section").map { |string| string.scan(/(?=^\{((?:[^{}]++|\{\g<1>\})++)\})/)[0]}
##! string[/\((?>[^)(]+|\g<0>)*\)/] 

##! just matches the content between first { and first }: [/^\{(.*?)\}/]

if countEnds == countBegins
  puts "\n" ##!delete
  print blocks 
  puts "\n"
else 
  print "counts of environment begins: \n" 
  print countBegins 
  puts "\n\n"
  print "counts of environment ends:" 
  print countEnds
  abort("File not in LiSci LaTeX format! Unequal numbers of begin and end environment!")
  ## maybe raise instead of abort, and same above
end

